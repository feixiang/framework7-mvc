// requirejs setting
require.config({
	paths: {
		Framework7: '../libs/framework7/js/framework7',
		text: '../libs/text', // text plugin to load template html
		template: '../template/template.html'
	},
	shim: {
		'Framework7': {
			exports: 'Framework7'
		}
	}
});

define('app', ['router', 'Framework7'], function(Router, Framework7) {
	var App = {
		init: function() {
			// init f7 and export to global
			window.$$ = Dom7;
			window.myApp = new Framework7();
			// add views
			window.mainView = myApp.addView('.view-main', {
				dynamicNavbar: true
			});
		}
	}

	App.init();
	// router loads every controller via data-page attribute in page
	Router.init();

	return {
		App: App
	};
});