define(['util/TemplateManager', 'service/About'], function(TemplateManager, AboutService) {
	function init() {
		Controller.init();
	}

	var Controller = {
		init: function() {
			AboutService.getInfo(function(data) {
				TemplateManager.render('#AboutTemplate', '#version', data);
			});
		}
	}

	return Controller;
});