define(['util/TemplateManager'], function(TemplateManager, Http) {
	function init() {
		Controller.init();
	}

	var Controller = {
		init: function() {
			this.bindEvents();
		},
		bindEvents: function() {
			var events = [{
				element: ".btn-alert",
				event: "click",
				handler: function() {
					myApp.alert('this is page1');
				}
			}];
			TemplateManager.bindEvents(events);
		}
	}

	return Controller;
});