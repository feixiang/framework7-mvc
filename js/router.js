define(function () {
    function init() {
        $$(document).on('pageBeforeInit', function (e) {
            var page = e.detail.page;
            load(page.name, page.query);
        });
    }

    function load(controller, query) {
        if (controller) {
            require(['controller/' + controller], function (controller) {
                controller.init(query);
            });
        }
    }

    return {
        init: init,
        load: load
    };
});