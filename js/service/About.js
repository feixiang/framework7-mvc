define(['util/Http'], function(Http) {
	var Service = {
		getInfo: function(callback) {
			Http.get('version', function(data) {
				callback(data);
			});
		}
	}
	return Service;
});