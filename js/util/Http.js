define(['util/Config'], function(Config) {
	var Http = {
		get: function(api, callback) {
			// use json data for test
			var url = "data/" + api + ".json";
			$$.get(url, function(ret) {
				ret = ret ? JSON.parse(ret) : '';
				typeof(callback === 'function') ? callback(ret): null;
			});
		},

		post: function(url, data, callback) {
			url = Config.api + url;
			$$.post(url, data, function(ret) {
				ret = ret ? JSON.parse(ret) : '';
				typeof(callback === 'function') ? callback(ret): null;
			});
		}
	}
	return Http;
})