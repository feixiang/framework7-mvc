define(['text!template'], function(template) {
	var TemplateManager = {
		init: function() {
			$$('body').append(template);
		},

		/**
		 * render templates 
		 * @param {Object} id
		 * @param {Object} output
		 * @param {Object} data
		 * @param {Object} callback
		 */
		render: function(id, output, data, callback) {
			var template = $$(id).html();
			var compiled = Template7.compile(template);
			var html = compiled(data);
			$$(output).html(html);
		},

		/**
		 * delegate events
		 * @param bindings
		 */
		bindEvents: function(bindings) {
			for (var i in bindings) {
				if (bindings[i].selector) {
					$$(bindings[i].element)
						.on(bindings[i].event, bindings[i].selector, bindings[i].handler);
				} else {
					$$(bindings[i].element)
						.on(bindings[i].event, bindings[i].handler);
				}
			}
		}
	};
	// we need to prepend templates to page before we render it
	TemplateManager.init();
	
	return TemplateManager;
});