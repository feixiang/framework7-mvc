define(function() {
	var Util = {
		initUpdate: function() {
			// check for update
			window.addEventListener('load', function(e) {
				window.applicationCache.addEventListener('updateready', function(e) {
					if (window.applicationCache.status === window.applicationCache.UPDATEREADY) {
						myApp.confirm('new version found,update?', function() {
							window.location.reload();
						});
					} else {
						// no updates available.
					}
				}, false);
			}, false);
		}
	}

	return Util;
});